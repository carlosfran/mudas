class Planta():
    def __init__(self, nome_popular, nome_cientifico, categoria,
                 descricao, estoque):
        self.id = 0
        self.nome_popular = nome_popular
        self.nome_cientifico = nome_cientifico
        self.categoria = categoria
        self.descricao = descricao
        self.estoque = estoque

    def getId(self):
        return self.id

    def setId(self, id):
        self.id = id

    def getEstoque(self):
        return self.estoque

    def setEstoque(self, qtde):
        self.estoque = qtde
