class UsuarioDAO():
    def __init__(self, con):
        self.con = con

    # CRUD - Create, Retrieve, Update, Delete
    def inserir(self, usuario):
        try:
            sql = "INSERT INTO Usuario(email, senha, nome, sobrenome," \
                  " dtnasc, logradouro, cidade, uf, tipo) " \
                  "VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)"

            cursor = self.con.cursor()
            cursor.execute(sql, (usuario.email, usuario.senha, usuario.nome,
                                 usuario.sobrenome, usuario.dtNasc,
                                 usuario.logradouro,
                                 usuario.cidade, usuario.uf, usuario.tipo))
            self.con.commit()
            codigo = cursor.lastrowid
            return codigo
        except:
            return 0


    def autenticar(self, email, senha):
        try:
            sql = "SELECT * FROM Usuario WHERE email=%s AND senha=%s"

            cursor = self.con.cursor()
            cursor.execute(sql, (email, senha))

            usuario = cursor.fetchone() # lastrowid, fetchone, fetchall
            return usuario
        except:
            return None
