class PlantaDAO():
    def __init__(self, con):
        self.con = con

    # CRUD - Create, Retrieve, Update, Delete
    def inserir(self, planta):
        try:
            sql = "INSERT INTO Planta(nome_popular, nome_cientifico, " \
                  "categoria, descricao, estoque) VALUES (%s, %s, %s, %s, %s)"

            cursor = self.con.cursor()
            cursor.execute(sql, (planta.nome_popular, planta.nome_cientifico,
                                 planta.categoria, planta.descricao, planta.estoque))
            self.con.commit()
            codigo = cursor.lastrowid
            return codigo
        except:
            return 0

    def listar(self, codigo=None):
        try:
            cursor = self.con.cursor()
            if codigo != None:
                # pegar somente uma planta
                sql = "SELECT * FROM Planta WHERE codigo=%s"
                cursor.execute(sql, (codigo,))
                planta = cursor.fetchone()
                return planta
            else:
                # pegar todas as plantas
                sql = "SELECT * FROM Planta"
                cursor.execute(sql)
                plantas = cursor.fetchall()
                return plantas
        except:
            return None















