-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mudasdb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema mudasdb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `mudasdb` DEFAULT CHARACTER SET utf8 ;
USE `mudasdb` ;

-- -----------------------------------------------------
-- Table `mudasdb`.`Usuario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mudasdb`.`Usuario` (
  `codigo` BIGINT NOT NULL AUTO_INCREMENT,
  `email` VARCHAR(250) NOT NULL,
  `senha` VARCHAR(250) NOT NULL,
  `nome` VARCHAR(100) NOT NULL,
  `sobrenome` VARCHAR(100) NOT NULL,
  `dtnasc` DATE NULL,
  `logradouro` VARCHAR(250) NOT NULL,
  `cidade` VARCHAR(100) NOT NULL,
  `uf` VARCHAR(2) NOT NULL,
  `tipo` INT NOT NULL,
  PRIMARY KEY (`codigo`),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mudasdb`.`Planta`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mudasdb`.`Planta` (
  `codigo` BIGINT NOT NULL AUTO_INCREMENT,
  `nome_popular` VARCHAR(100) NOT NULL,
  `nome_cientifico` VARCHAR(100) NULL,
  `categoria` VARCHAR(50) NOT NULL,
  `descricao` VARCHAR(250) NULL,
  `estoque` INT NOT NULL,
  PRIMARY KEY (`codigo`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mudasdb`.`Saida`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mudasdb`.`Saida` (
  `codigo` BIGINT NOT NULL AUTO_INCREMENT,
  `dtsaida` DATE NOT NULL,
  `Usuario_codigo` BIGINT NOT NULL,
  `Planta_codigo` BIGINT NOT NULL,
  PRIMARY KEY (`codigo`),
  INDEX `fk_Saida_Usuario_idx` (`Usuario_codigo` ASC) ,
  INDEX `fk_Saida_Planta1_idx` (`Planta_codigo` ASC) ,
  CONSTRAINT `fk_Saida_Usuario`
    FOREIGN KEY (`Usuario_codigo`)
    REFERENCES `mudasdb`.`Usuario` (`codigo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Saida_Planta1`
    FOREIGN KEY (`Planta_codigo`)
    REFERENCES `mudasdb`.`Planta` (`codigo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
