from flask import Flask, g, render_template,\
    request, redirect, url_for, flash, session

import mysql.connector

# from models.arquivo import Classe
from models.usuario import Usuario
from models.usuarioDAO import UsuarioDAO
from models.planta import Planta
from models.plantaDAO import PlantaDAO


app = Flask(__name__)
app.secret_key = "senha123"

DB_HOST = "localhost"
DB_USER = "root"
DB_PASS = "root"
DB_NAME = "mudasdb"

app.auth = {
    # acao: { perfil:permissao }
    'painel': {0:1, 1:1},
    'logout': {0:1, 1:1},
    'cadastrar_planta': {0:1, 1:1},
    'listar_planta': {0:1, 1:1}
}

@app.before_request
def autorizacao():
    acao = request.path[1:]
    acao = acao.split('/')
    if len(acao)>=1:
        acao = acao[0]

    acoes = app.auth.keys()
    if acao in list(acoes):
        if session.get('logado') is None:
            return redirect(url_for('login'))
        else:
            tipo = session['logado']['tipo']
            if app.auth[acao][tipo]==0:
                return redirect(url_for('painel'))

def get_db():
    db = getattr(g, '_database', None)
    if db is None:
        db = g._database = mysql.connector.connect(
            host=DB_HOST,
            user=DB_USER,
            password=DB_PASS,
            database=DB_NAME
        )
    return db


@app.teardown_appcontext
def close_connection(exception):
    db = getattr(g, '_database', None)
    if db is not None:
        db.close()


@app.route('/')
def index():
    return render_template("index.html")


@app.route('/cadastrar', methods=['GET', 'POST'])
def cadastrar():
    if request.method == "POST":
        # valor = request.form['campoHTML']
        nome = request.form['nome']
        sobrenome = request.form['sobrenome']
        dtnasc = request.form['dtnasc']
        logradouro = request.form['logradouro']
        uf = request.form['uf']
        cidade = request.form['cidade']
        email = request.form['email']
        senha = request.form['senha']

        usuario = Usuario(email, senha, nome, sobrenome,
                 dtnasc, logradouro, cidade, uf, 0)

        dao = UsuarioDAO(get_db())
        codigo = dao.inserir(usuario)

        if codigo > 0:
            flash("Cadastrado com sucesso! Código %d" % codigo, "success")
        else:
            flash("Erro ao cadastrar!", "danger")

    vartitulo = "Cadastro"
    return render_template("cadastrar.html", titulo=vartitulo)


@app.route('/cadastrar_planta', methods=['GET', 'POST'])
def cadastrar_planta():
    if request.method == "POST":
        nome_popular = request.form['nome_popular']
        nome_cientifico = request.form['nome_cientifico']
        descricao = request.form['descricao']
        categoria = request.form['categoria']
        estoque = request.form['estoque']

        planta = Planta(nome_popular, nome_cientifico, categoria,
                 descricao, estoque)

        dao = PlantaDAO(get_db())
        codigo = dao.inserir(planta)

        if codigo > 0:
            flash("Cadastrado com sucesso! Código %d" % codigo, "success")
        else:
            flash("Erro ao cadastrar!", "danger")

    vartitulo = "Cadastro de Planta"
    return render_template("planta-cadastrar.html", titulo=vartitulo)

@app.route('/listar_planta', methods=['GET',])
def listar_planta():
    dao = PlantaDAO(get_db())
    plantas_db = dao.listar()
    return render_template("planta-listar.html", plantas=plantas_db)


@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == "POST":
        email = request.form['email']
        senha = request.form['senha']

        # Verificar dados
        dao = UsuarioDAO(get_db())
        usuario = dao.autenticar(email, senha)

        if usuario is not None:
            session['logado'] = {
                'codigo': usuario[0],
                'nome': usuario[3],
                'sobrenome': usuario[4],
                'email': usuario[1],
                'tipo': usuario[9]
            }
            return redirect(url_for('painel'))
        else:
            flash("Erro ao efetuar login!", "danger")

    return render_template("login.html", titulo="Login")


@app.route('/logout')
def logout():
    session['logado'] = None
    session.clear()
    return redirect(url_for('index'))


@app.route('/painel')
def painel():
    return render_template("template.html", titulo="Painel")


if __name__=='__main__':
    app.run(host="0.0.0.0", port=80, debug=True)